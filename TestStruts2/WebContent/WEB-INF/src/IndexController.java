import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Action(value="initialize", results={@Result(name="success", location="/institutions-index.jsp")})

public class IndexController extends ActionSupport {

	@Override
    public String execute() throws Exception
    {
        return SUCCESS;
    }
}