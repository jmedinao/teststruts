import java.util.Collection;

import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Validateable;
import com.opensymphony.xwork2.ValidationAwareSupport;

@SuppressWarnings("serial")
@Results({
	@Result(name="success", type="redirectAction", params = {"actionName" , "institutions"})
})
public class InstitutionsController extends ValidationAwareSupport implements ModelDriven<Object>, Validateable{

	private Institution model = new Institution();
    private String id;
    
    private Collection<Institution> list;
    private InstitutionsService institutionsService = new InstitutionsService();
	
    // POST /institutions
    public HttpHeaders create() {
        //log.debug("Create new order {}", model);
    	
    	System.out.println("Create new order");
    	
        //ordersService.save(model);
        //addActionMessage("New order created successfully");
        return new DefaultHttpHeaders("success").setLocationId(model.getId());
    }
    
    
    // GET /institutions/1
    public HttpHeaders show() {
        return new DefaultHttpHeaders("show");
    }
    
    // GET /institutions
    public HttpHeaders index() {
    	list = institutionsService.getAll();
        return new DefaultHttpHeaders("index").disableCaching();
    }
    
	
	
    public void validate() {
    	//if (model.getClientName() == null || model.getClientName().length() ==0) {
    	//    addFieldError("clientName", "The client name is empty");
    	//}
    }
	
	 // GET /orders/new
    public String editNew() {
        //model = new Order();
        return "editNew";
    }

    
    public void setId(String id) {
        if (id != null) {
            this.model = institutionsService.get(id);
        }
        this.id = id;
    }
    
    public Object getModel() {
        return (list != null ? list : model);
    }
	
	
}
