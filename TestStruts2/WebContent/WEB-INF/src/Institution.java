
public class Institution {

	String id;
	String headQuearter;
	String acronym;
	String name;
	String type;
	String country;
	String city;
	String urlPartner;
	
	public Institution() {}
	
	public Institution(String id, String headQuearter,
			String acronym, String name, String type, String country,
			String city, String urlPartner) {
		super();
		this.id = id;
		this.headQuearter = headQuearter;
		this.acronym = acronym;
		this.name = name;
		this.type = type;
		this.country = country;
		this.city = city;
		this.urlPartner = urlPartner;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHeadQuearter() {
		return headQuearter;
	}

	public void setHeadQuearter(String headQuearter) {
		this.headQuearter = headQuearter;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getUrlPartner() {
		return urlPartner;
	}

	public void setUrlPartner(String urlPartner) {
		this.urlPartner = urlPartner;
	}
	
	
	
	
	
}
