import java.util.Properties;
import java.io.*; 

public class DataBaseText {

	private Properties properties_read;
	private Properties properties_write;
	private FileOutputStream out;
	private String path_file;

	public DataBaseText(String file)
	{
		
		path_file = file;
		properties_read = new Properties();

		try {
			FileInputStream in = new FileInputStream(file);
			properties_read.load(in);
			in.close();
		} catch (Exception e)
		{ 
			System.out.println("DataBaseText(): " + e.toString());
		}

	}

	public void store()
	{
		try {
			out = new FileOutputStream(path_file);
			properties_read.store(out, "Do not modify, this file is generated automatically" );
		}
		catch(Exception e)
		{
			System.out.println("DataBaseText(): " + e.toString());
		}
	}
	
	public String getProperty(String prop)
	{
		String p = properties_read.getProperty(prop);
		return p;
	}

	public void setProperty(String prop, String value)
	{
		properties_read.setProperty(prop, value);
	}
		
}
