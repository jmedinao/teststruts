import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class InstitutionsService {

	public static String dataBasePath = "dataBase.txt";
	
	DataBaseText db;
	
	private static int rows;
	private static Map<String,Institution> institutions = new HashMap<String,Institution>(); //orders
	
	public InstitutionsService() {
		
		//check if the database file exists
		try {
			
			FileReader fr = new FileReader(dataBasePath);
	
			try { 
				fr.close(); 
			} catch(Exception e2) {
				
			}
			
		} catch (FileNotFoundException e) {
			
			System.out.println("El Archivo no existe");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			try {
				
				FileWriter fw = new FileWriter(dataBasePath, true);
				
				PrintWriter pw = new PrintWriter(fw);
				pw.print("db.rows=0");
				
				pw.close();
				fw.close();
				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
		
		if(db == null) {
			db = new DataBaseText(dataBasePath);
		}
		
		String str_rows = db.getProperty("db.rows");
		rows = Integer.parseInt(str_rows);
		
		//load the rows in the hashmap
		for(int i = 0; i < rows; i++) {
			
			String rowloaded = db.getProperty("row." + (i + 1));
					
			StringTokenizer st = new StringTokenizer(rowloaded, "|");
			
			String headQuearter = "";
			String acronym = "";
			String name = "";
			String type = "";
			String country = "";
			String city = "";
			String urlPartner = "";
			
			if(st.hasMoreElements())
				headQuearter = st.nextToken();
			
			if(st.hasMoreElements())
				acronym = st.nextToken();
			
			if(st.hasMoreElements())
				name = st.nextToken();
			
			if(st.hasMoreElements())
				type = st.nextToken();
				
			if(st.hasMoreElements())
				country = st.nextToken();
				
			if(st.hasMoreElements())
				city = st.nextToken();
				
			if(st.hasMoreElements())
				urlPartner = st.nextToken();
				
			institutions.put(i + "", new Institution(i + "", headQuearter, acronym, name, type, country, city, urlPartner));
			
		}
		
		
		
	}
	
	public Institution get(String id) {
        return institutions.get(id);
    }
	
	public List<Institution> getAll() {
        return new ArrayList<Institution>(institutions.values());
    }
	
	/*
	public static void main(String args[]) {
		
		InstitutionsService is = new InstitutionsService();
		System.out.println("rows: " + is.getAll());
		
	}
	*/
	

}
