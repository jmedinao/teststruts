
<%@taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Demo</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/app.css" rel="stylesheet">

<title>Inicio</title>
</head>
<body>


   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
          </button>
          <a class="navbar-brand" href="#">Test Struts</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
           
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
  
	<div class="container">

      <div class="starter-template">
      
      <p class="lead"><br><br><br> </p>
   
   	<s:form method="post" action="%{#request.contextPath}/initialize" cssClass="form-horizontal" theme="simple">
		
		
			<div class="form-group">
	                 
	                <div class="form-group">
	                    <div class="col-sm-offset-2 col-sm-4">
	                  
	                        <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar el Test</button>
	                    </div>
	                </div>
	        </div>
	                
		</s:form>
	
   
   
      </div>

    </div><!-- /.container -->


	
	
	       
   
	        
</body>
</html>